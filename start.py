#https://papermc.io/ci/job/Paper-1.16/lastSuccessfulBuild/artifact/
import os
from configparser import ConfigParser
import inquirer

width = os.get_terminal_size().columns

config = ConfigParser()

iniFileName = "UpdaterConfig.ini"

config['server'] = {
	'serverType': 'paper',
	'serverVersion': '1.16'
}

def cls():
    os.system('cls' if os.name=='nt' else 'clear')

def Header():
	s = ''' _____                      _    _           _       _            
|  __ \                    | |  | |         | |     | |           
| |__) |_ _ _ __   ___ _ __| |  | |_ __   __| | __ _| |_ ___ _ __ 
|  ___/ _` | '_ \ / _ \ '__| |  | | '_ \ / _` |/ _` | __/ _ \ '__|
| |  | (_| | |_) |  __/ |  | |__| | |_) | (_| | (_| | ||  __/ |   
|_|   \__,_| .__/ \___|_|   \____/| .__/ \__,_|\__,_|\__\___|_|   
           | |                    | |                             
   v.1.0   |_|   by: JoShMiQueL   |_|  bitbucket.org/JoShMiQueL   '''

	print('\n'.join(l.center(width) for l in s.splitlines()))
	print(''.join('\n' for q in range(1,3)))

cls()

def LoadConfig():
	if (not os.path.isfile(f'./{iniFileName}')):
		print(f"The file '{iniFileName}' does not exist, creating...")
		with open(f'./{iniFileName}', 'w') as f:
			config.write(f)
	else:
		config.read(iniFileName)
		print(f"File '{iniFileName}' found, reading...")
		print("")


def checkPaperVersion():
	validServerTypes = {'craftbukkit', 'paper', 'spigot'}
	config.read(iniFileName)
	if (not config.get('server', 'serverType') in validServerTypes):
		questions = [
		    inquirer.List(
		        "size",
		        message="What size do you need?",
		        choices=["Jumbo", "Large", "Standard", "Medium", "Small", "Micro"],
		    ),
		]

		answers = inquirer.prompt(questions)
		return
	serverJarsApi = "https://serverjars.com/api"
	lastVersionJson = f"{serverJarsApi}/fetchLatest/{config.get('server', 'serverType')}"
	print(lastVersionJson)





def start():
	Header()
	LoadConfig()
	checkPaperVersion()

# start()
questions = [
    inquirer.List(
        "size",
        message="What size do you need?",
        choices=["Jumbo", "Large", "Standard", "Medium", "Small", "Micro"],
    ),
]

answers = inquirer.prompt(questions)
print(answers['size'])