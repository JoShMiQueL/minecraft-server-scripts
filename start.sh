#!/bin/bash
#https://papermc.io/ci/job/Paper-1.16/lastSuccessfulBuild/artifact/

for i in $(seq 100)
do
   echo ""
done

paperJenkins="https://papermc.io/ci"
downloadLink="$paperJenkins/job/Paper-$serverVersion/lastSuccessfulBuild/artifact/paperclip.jar"

function downloadPaper() {
	echo "Downloading Paper $serverVersion..."
	wget --no-check-certificate --quiet "$downloadLink" -O paper.jar
	echo "Download completed!"
}

function checkPaperVersion() {
	serverJarsApi="https://serverjars.com/api"
	lastVersionInfo="$serverJarsApi/fetchLatest/$serverType"
	if [ ! -f "./last-build" ]; then
		echo "The \"last-build\" file doesn't exist, creating..."
		echo "" > "./last-build"
	fi
	curl -s $lastVersionInfo > last-build
	xd=$(grep "built" ./last-build)
	echo "$xd"
}

checkPaperVersion